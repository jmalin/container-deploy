ARG FROM_IMAGE=usgs/node:latest

FROM ${FROM_IMAGE}

WORKDIR /

USER root

RUN /bin/bash --login -c "\
  yum update -y && \
  yum install -y git \
"

USER usgs-user