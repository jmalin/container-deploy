#!/usr/bin/env groovy

node {
  def CONFIG
  def DEPLOY_DIR
  def EXPORTS
  def TARGET_HOSTS

  try {
    stage('Initialize') {
      cleanWs()

      DEPLOY_DIR="/tmp/${params.APP_NAME}"

      dir('generic') {
        checkout scm
      }

      dir('custom') {
        def branch = 'origin/master'

        if (params.GIT_BRANCH != '') {
          branch = params.GIT_BRANCH
        }

        checkout(
          changelog: false,
          poll: false,
          scm: [
            $class: 'GitSCM',
            branches: [[name: branch]],
            doGenerateSubmoduleConfigurations: false,
            extensions: [],
            submoduleCfg: [],
            userRemoteConfigs: [
              [
                credentialsId: 'innersource-hazdev-cicd',
                url: params.APP_REPOSITORY
              ]
            ]
          ]
        )
      }

      dir('config') {
        checkout(
          changelog: false,
          poll: false,
          scm: [
            $class: 'GitSCM',
            branches: [[name: '*/master']],
            doGenerateSubmoduleConfigurations: false,
            extensions: [],
            submoduleCfg: [],
            userRemoteConfigs: [
              [
                credentialsId: 'innersource-hazdev-cicd',
                url: 'https://${GITLAB_INNERSOURCE_REGISTRY}/ghsc/hazdev/jenkins.git'
              ]
            ]
          ]
        )

        CONFIG = readJSON file: 'config.json'
        CONFIG = CONFIG[params.ENVIRONMENT.toLowerCase()]
      }

      TARGET_HOSTS = CONFIG.container.keySet()

      // Build string of exports to execute on remote servers
      EXPORTS='';
      params.keySet().each { key ->
        def value = params[key]

        if (key != '' && value != '') {
          if (EXPORTS == '') {
            EXPORTS = "export ${key}=${value}"
          } else {
            EXPORTS="${EXPORTS} &&\n                  export ${key}=${value}"
          }
        }
      }
    }

    stage('Prepare') {
      def TASKS = [:]

      TARGET_HOSTS.each { host ->
        TASKS["${host}-prepare"] = {
          // Create the directory for the deployment scripts
          sh """
            ssh -l ${JENKINS_DEPLOY_USER} ${host} \
              '
                mkdir -p ${DEPLOY_DIR};
              '
          """
          // Copy deployment scripts to server
          sh """
            scp \
              ./custom/custom.config.sh \
              ./custom/custom.funcs.sh \
              ./custom/${params.APP_NAME}.yml \
              ./generic/default.config.sh \
              ./generic/default.funcs.sh \
              ./generic/deploy.sh \
              ${JENKINS_DEPLOY_USER}@${host}:${DEPLOY_DIR}/.
          """
        }
      }

      parallel TASKS
    }

    stage('Deploy') {
      def TASKS = [:]

      TARGET_HOSTS.each { host ->
        TASKS["${host}-deploy"] = {
          echo "Deploying stack on ${host} ..."

          withCredentials([usernamePassword(
              credentialsId: 'innersource-hazdev-cicd',
              passwordVariable: 'REGISTRY_PASSWORD',
              usernameVariable: 'REGISTRY_USERNAME')]) {
            sh """
              ssh -l ${JENKINS_DEPLOY_USER} ${host} \
                '
                  ${EXPORTS} &&
                  export REGISTRY=${GITLAB_INNERSOURCE_REGISTRY} &&
                  export STACK_NAME=${params.APP_NAME} &&
                  export TARGET_HOSTNAME=${host} &&
                  docker login ${GITLAB_INNERSOURCE_REGISTRY} \
                    -u ${REGISTRY_USERNAME} \
                    -p ${REGISTRY_PASSWORD} &&
                  ${DEPLOY_DIR}/deploy.sh
                '
            """
          }
        }
      }

      parallel TASKS
    }

    stage('Cleanup') {
      def TASKS = [:]

      TARGET_HOSTS.each { host ->
        TASKS["${host}-cleanup"] = {
          echo "Cleaning up ${host} ..."
          sh """
            ssh -l ${JENKINS_DEPLOY_USER} ${host} \
              '
                rm -rf ${DEPLOY_DIR};
              '
          """
        }
      }

      // parallel TASKS
    }

    stage('Next Job') {
      if (params.NEXT_JOB && params.NEXT_JOB != '') {
        def nextParams = []

        params.each { name, value ->
          def className = value.getClass().toString()
          def nextParam

          // Do not forward these parameters to the next job
          if (name == 'NEXT_JOB' || name == 'GIT_BRANCH' ||
              name == 'APP_REPOSITORY' || name == 'APP_NAME') {
            return // i.e. continue
          }

          switch(className) {
            case 'class java.lang.String':
              nextParam = string('name': name, 'value': value)
              break
            case 'class java.lang.Boolean':
              nextParam = booleanParam('name': name, 'value': value)
              break
          }

          if (nextParam) {
            nextParams.push(nextParam)
          }
        }

        build(
          job: params.NEXT_JOB,
          parameters: nextParams,
          propagate: false,
          wait: false
        )
      } else {
        echo 'No subsequent job specified'
      }
    }
  } catch (e) {
    mail to: 'gs-haz_dev_team_group@usgs.gov',
      from: 'noreply@jenkins',
      subject: "Jenkins Failed: ${env.JOB_NAME} [${env.BUILD_NUMBER}]'",
      body: "Project build (${BUILD_TAG}) failed '${e}'"

    currentBuild.result = 'FAILURE'
    throw e
  } finally {
    return this;
  }
}
