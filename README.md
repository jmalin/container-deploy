# Container Deploy

This project provides a generic container deployment pipeline. Individual
container applications can invoke this project with some custom
configuration options in order to deploy themselves into a configured
environment. See below for details...

## Using this generic deployment

### Create a new custom deployment Git project on code.chs.usgs.gov

The project should be called "app-name.git" within the appropriate group
hierarchy. We will refer to "app-name" as `APP_NAME` throughout this
documentation and within the referenced scaffold pipeline configuration
(see below).

This project should contain the following files:
- **`<APP_NAME>.yml`** This file is a docker-compose file used to specify the
  deployment configuration for the application. All generic build parameters are
  made available as environment variables at deploy time. Any custom build
  parameters need to be explicitly exported as environment variables in the
  `custom.config.sh` file (see below). This file may further make a subset of
  these variables available as environment variables within the container
  runtime by using the `environment` section.
- **`custom.config.sh`** This file contains custom configuration for the
  application deployment. Any custom build parameters should be exported to
  the environment if they are necessary to be used by the stack deployment.
  Do not execute any logic in this file, for more complex customizations,
  consider using the `preStackDeployHook` lifecycle hook.
- **`custom.funcs.sh`** This file contains custom lifecycle hook function
  definitions. See "Lifecycle Hooks" below. Not all lifecycle hooks need be
  defined. Fallbacks are provided by the generic project.

### Create a new custom deployment job in Jenkins

Use the `DevOps/container-deploy` project as a basis. You can create the
new job by clicking "New Item", enter a job name, then type in
"/DevOps/container-deploy" into the text box labeled "Copy from". This will
create the basic job scaffold. It requires customizations...

Update the job description to be meaningful for the current deployment
project.

Add additional "Build Parameters" as necessary. The generic deployment
will not use these parameters, but application-specific customizations may do
so. Build parameters are all provided as environment variables to the
`deploy.sh` script. Build parameters are also provided to any downstream
`NEXT_JOB` if specified (with some exclusions). Build parameters will appear
on the Jenkins screen when a user opts to manually deploy the project. They
appear in the order defined in the configuration page, so re-order these as
appropriate for the current application (drag the scrubber in the upper-left
corner of the build parameter box to re-order).

Do not change any thing else unless you really know what you're doing.

## Lifecycle Hooks

The generic build is customized by exposing a set of lifecycle hooks called
at various times throughout the deploy process. See the lifecycle hooks section
near the bottom of the default.funcs.sh file contained within this project.