##
# Default configuration file. All configured variables should be exported. All
# configured variables should defer to existing configured values i.e.
#
# VAR=${VAR:-defaultValue};
#
# or
#
# if [ -z "${VAR}" ]; then
#   VAR='value';
# fi
#
##

# Implicitly defines the YML file used for stackd deployment
export APP_NAME=${APP_NAME:-'generic-application'};

# How long to wait for services to become healthy after deploy
export MAX_WAIT=${MAX_WAIT:-3600};

# Registry in which one might find deployable images
export REGISTRY=${REGISTRY:-'code.chs.usgs.gov:5001'};

# Names the stack in the swarm
export STACK_NAME=${STACK_NAME:-'generic-stackname'};

# Host for hazdev-router to proxy to
export TARGET_HOSTNAME=${TARGET_HOSTNAME:-$(hostname)};

if [ ${#TARGET_HOSTS[@]} -eq 0 ]; then
  # TODO :: Read these from external config somehow?
  case $TARGET_HOSTNAME in
    dev01*)
      export TARGET_HOSTS=(
        'dev01-container01.cr.usgs.gov'
        'dev01-container02.cr.usgs.gov'
        'dev01-container03.cr.usgs.gov'
      );
    ;;
    dev02*)
      export TARGET_HOSTS=(
        'dev02-container01.cr.usgs.gov'
        'dev02-container02.cr.usgs.gov'
        'dev02-container03.cr.usgs.gov'
      );
    ;;
    prod01*)
      export TARGET_HOSTS=(
        'prod01-container01.cr.usgs.gov'
        'prod01-container02.cr.usgs.gov'
        'prod01-container03.cr.usgs.gov'
      );
    ;;
    prod02*)
      export TARGET_HOSTS=(
        'prod02-container01.cr.usgs.gov'
        'prod02-container02.cr.usgs.gov'
        'prod02-container03.cr.usgs.gov'
      );
    ;;
    *)
      export TARGET_HOSTS=(
        "${TARGET_HOSTNAME}"
      );
    ;;
  esac
fi

# This REALLY SHOULD be overwritten by custom application
if [ ${#SERVICE_MAP[@]} -eq 0 ]; then
  export SERVICE_MAP=(
    '/some/path':'someservice'
    '/some/other/path':'otherservice'
  );
fi