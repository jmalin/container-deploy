TRUTHY=${TRUTHY:-'true'};
FALSY=${FALSY:-''};
# Set to non-empty string to turn on debugging
DEBUG=${DEBUG:-${FALSY}};

##
# Prints the given message if debugging is currently turned on
#
# @param $1 message {String}
#     The message to print
##
debug () {
  if [ -n "${DEBUG}" ]; then
    # Log to stderr since stdout is gobbled up
    echo "[DEBUG $(date -u)] $1 " 1>&2;
  fi
}

##
# Checks if the named function exists in the current BASH context.
#
# @param $1 funcName {String}
#     The name of the function to check
#
# @return
#     ${TRUTHY} if the function exists, ${FALSY} otherwise.
##
functionExists () {
  local funcName=$1;
  local typeOf=$(type -t $funcName);
  local result=$?;

  if [[ $result -eq 0 && "${typeOf}" == 'function' ]]; then
    echo ${TRUTHY};
  else
    echo ${FALSY};
  fi
}

##
# Checks for existing "config" and "server" configurations for the given
# `appName` application. If they exist, check the values of each against the
# content in the given `newConfigFile` and `newServerFile` (respectively).
#
# If either "config" or "server" configuration does not exist or is different
# from its corresponding new value, then this function returns ${TRUTHY},
# otherwise this function returns ${FALSY}.
#
# @param $1 {String}
#      Name of application for which to check the configurations
# @param $2 {String}
#      Name of the file containing the new "config" configuration
# @param $3 {String}
#      Name of the file containing the new "server" configuration
#
# @return {Boolean}
#      ${TRUTHY} if the configs either don't already exist, or differ from
#      their corresponding new values. ${FALSY} otherwise.
##
if [ ! $(functionExists configsDiffer) ]; then
configsDiffer () {
  local appName=$1;
  local newConfigFile=$2;
  local newServerFile=$3;

  local dir=$(dirname $0);

  if [[
    -z "${appName}" ||
    -z "${newConfigFile}" ||
    -z "${newServerFile}"
  ]]; then
    echo "Usage: configsDiffer <appName> <configFile> <serverFile>" >&2;
    echo ${TRUTHY}; # Bad usage assumes configs differ b/c slower but safer
    return;
  fi

  local curConfigFile="${dir}/${appName}-current.conf";
  local curServerFile="${dir}/${appName}-current.server";

  # Check each configuration exists
  local curConfigName=$(getConfigName hazdev-router_nginx config--${appName});
  local curServerName=$(getConfigName hazdev-router_nginx server--${appName});

  if [[ -z "${curConfigName}" || -z "${curServerName}" ]]; then
    echo "${TRUTHY}";
    return;
  fi

  # Check each configuration matches
  readConfig ${curConfigName} > ${curConfigFile};
  readConfig ${curServerName} > ${curServerFile};

  configDiffs=$(\
    diff \
      --ignore-matching-lines='^\s*#' \
      ${curConfigFile} \
      ${newConfigFile} \
    | wc -l
  );

  serverDiffs=$(\
    diff \
      --ignore-matching-lines='^\s*#' \
      ${curServerFile} \
      ${newServerFile} \
    | wc -l
  );

  if [[ $configDiffs -ne 0 || $serverDiffs -ne 0 ]]; then
    echo ${TRUTHY};
  else
    echo ${FALSY};
  fi
}
fi

##
# Get the full name of a configuration property containing the `partialName`
# that is attached to the given `serviceName`.
#
# @param $1 {String}
#      The name of the service to check for the attached configuration
# @param $2 {String}
#      The partial name of the configuration to look for
#
# @return {String}
#      The full name of the matched configuration property
##
if [ ! $(functionExists getConfigName) ]; then
getConfigName () {
  local serviceName=$1;
  local partialName=$2;

  if [[ -z "${serviceName}" || -z "${partialName}" ]]; then
    echo 'Usage: getConfigName <serviceName> <partialName>' >&2;
    echo '';
    return;
  fi

  docker service inspect \
      --format '
        {{ range $i, $v := .Spec.TaskTemplate.ContainerSpec.Configs }}
          {{ println $v.ConfigName }}
        {{ end }}
      ' \
      ${serviceName} \
    | grep ${partialName}
}
fi

##
# Finds the first published port for the given service.
#
# @param $1 serviceName {String}
#      The name of the service for which to return the port
#
# @return {String}
#      The first port number exposed on the service or an empty string if
#      no ports are exposed.
##
if [ ! $(functionExists getPublishedPort) ]; then
getPublishedPort () {
  local serviceName=$1;
  local format='--format={{(index .Endpoint.Ports 0).PublishedPort}}';

  if [ -z "${serviceName}" ]; then
    echo "Usage: getPublishedPort <serviceName>" >&2;
    echo '';
    return;
  fi

  local port=$(docker service inspect "$format" $serviceName 2> /dev/null);

  echo $port;
}
fi

##
# @param $1 {String}
#      The name of the configuration property to read
#
# @return {String}
#      The text value of the configuration property read
##
if [ ! $(functionExists readConfig) ]; then
readConfig () {
  local configName=$1;

  if [ -z "${configName}" ]; then
    echo "Usage: readConfig <configName>" >&2;
    echo '';
    return;
  fi

  docker config inspect --format '{{ json .Spec.Data }}' ${configName} \
    | sed 's/"//g' \
    | base64 --decode;
}
fi

##
# @param $1 {String}
#     The action to perform. Either "--update", "--add", or "--remove".
# @param $@ {Mixed...}
#     Action-specific parameters to be passed to the underlying action.
#
##
if [ ! $(functionExists routerConfig) ]; then
routerConfig () {
  if [[ $1 == "--update" || "$1" == "--add" ]]; then
    GREP_FOR='hazdev-router_update-config';
  elif [[ "$1" == "--remove" ]]; then
    GREP_FOR='hazdev-router_remove-config';
  else
    echo "Usage: routerConfig [--add|--update|--remove] [args...]";
    exit -1;
  fi

  # Dump the first argument so we can pass everything else later
  shift;

  # Get the name of the configuration property containing the update code
  CONFIG_NAME=$(getConfigName hazdev-router_nginx ${GREP_FOR});

  # Get the configuration value
  CONFIG_SCRIPT=$(readConfig ${CONFIG_NAME});

  /bin/bash \
    -c "${CONFIG_SCRIPT}" \
    routerConfig \
    $@;
}
fi

##
# Updates the hazdev-router project to point to the recently deployed stack
#
# @param $1 stackName {String}
#     The name of the stack to point to
##
if [ ! $(functionExists updateRouting) ]; then
updateRouting () {
  local appName=$1; shift;
  local stackName=$1; shift;
  local serviceMap=$@;

  local dir=$(dirname $0);
  local stamp=$(date);

  local configFile="${dir}/${appName}.conf";
  local serverFile="${dir}/${appName}.server";

  debug "Re-routing traffic to ${stackName} stack.";
  echo "# Auto generated ${stamp} for ${stackName}" > $configFile;
  echo "# Auto generated ${stamp} for ${stackName}" > $serverFile;

  for service in ${serviceMap[@]}; do
    local name="${stackName}_$(echo $service | awk -F: '{print $2}')";
    local path="$(echo $service | awk -F: '{print $1}')";
    local port=$(getPublishedPort $name 2> /dev/null);

    if [ -z "${port}" ]; then
      # No port exposed. Continue.
      debug "No port exposed for ${name}. Not routing. Moving on.";
      continue;
    fi

    echo "upstream ${name} {" >> $configFile;
    for host in ${TARGET_HOSTS[@]}; do
      echo "  server ${host}:${port};" >> $configFile;
    done
    echo "}" >> $configFile;

    cat <<- EO_SERVER_SNIP >> $serverFile
      location ${path}/ {
        proxy_pass http://${name};
        proxy_set_header Host earthquake.usgs.gov;
        proxy_set_header X-Client-IP \$remote_addr;
      }
		EO_SERVER_SNIP
    # ^^ Note: TAB indentation required

  done

  if [ $(configsDiffer ${appName} ${configFile} ${serverFile}) ]; then
    debug "Updating configuration for ${appName}";
    routerConfig --update ${appName} ${configFile} ${serverFile};
  else
    debug "${appName} configuration not changed. Skipping router update.";
  fi
}
fi

##
# Waits for all stack services to become healthy after starting. This relies
# on each service replica to implement a meaningful HEALTHCHECK, otherwise
# "healthy" is equivalent to "running".
#
# @param $1 stackName {String}
#     The name of the stack that was removed.
#
##
if [ ! $(functionExists waitForStackHealthy) ]; then
waitForStackHealthy () {
  local stackName=$1;

  local complete=-1;
  local duration=0;
  local filter="--filter label=com.docker.stack.namespace=${stackName}";

  while [[ $complete -ne 0 && $duration -lt $MAX_WAIT ]]; do
    debug 'Waiting for services to become healthy...';
    sleep 5;
    let duration+=5;

    # Check each service in stack for health
    for status in $(docker service ls --format {{.Replicas}} ${filter}); do
      complete=$(echo $status | awk -F/ '{print $2 - $1}');

      if [ $complete -ne 0 ]; then
        # Found an unhealthy service, no need to keep checking. Keep waiting.
        break;
      fi
    done
  done

  # Show final service status(es)
  debug "$(docker service ls ${filter})";

  # Return the overall result
  if [ $complete -eq 0 ]; then
    echo ${TRUTHY};
  else
    echo ${FALSY};
  fi
}
fi

## ----------------------------------------------------------------------------
## Default lifecycle hooks. Only defined if not pre-defined by custom deploy
## pipeline scripts. These lifecycle hook functions each have full access
## to the current BASH execution context. Global variables, functions, etc...
## These lifecycle hooks should not expect any positional arguments.
## ----------------------------------------------------------------------------

##
# Lifecycle hook executed just prior to deploying the stack.
#
##
if [ ! $(functionExists preStackDeployHook) ]; then
  preStackDeployHook () {
    debug 'preStackDeployHook';
  };
fi

##
# Lifecycle hook executed after deploying the stack and it has become healthy.
#
##
if [ ! $(functionExists postStackDeployHook) ]; then
  postStackDeployHook () {
    debug 'postStackDeployHook';
  };
fi