#!/bin/bash

mkdir -p ~/.ssh
echo "$GITLAB_RUNNER_KEY" | tr -d '\r' > ~/.ssh/id_rsa
chmod 700 ~/.ssh
chmod 600 ~/.ssh/id_rsa
eval "$(ssh-agent -s)"
ssh-add ~/.ssh/id_rsa

IFS=$'\n'       # make newlines the only separator
set -f          # disable globbing
for host in $(cat < "${APP_NAME}_ARTIFACTS/${TARGET_FILE}"); 
do
  # add host fingerprint to known_hosts file
  ssh-keyscan -H $host >> ~/.ssh/known_hosts
  chmod 644 ~/.ssh/known_hosts

  echo "Deploying to $host ..."

  echo "Making deploy directory on $host ..."
  ssh gitlab-deploy@$host "mkdir -p ${DEPLOY_DIR}"

  echo "Copying artifacts to $host ..."
  scp -i ~/.ssh/id_rsa ${APP_NAME}_ARTIFACTS/custom/custom.config.sh \
    ${APP_NAME}_ARTIFACTS/custom/custom.funcs.sh \
    ${APP_NAME}_ARTIFACTS/custom/${APP_NAME}.yml \
    ${APP_NAME}_ARTIFACTS/generic/default.config.sh \
    ${APP_NAME}_ARTIFACTS/generic/default.funcs.sh \
    ${APP_NAME}_ARTIFACTS/generic/deploy.sh \
    ${APP_NAME}_ARTIFACTS/exports.sh \
  gitlab-deploy@$host:${DEPLOY_DIR}

  echo "SSH into $host and running deployment commands ..."

  ssh -n gitlab-deploy@$host "
      source ${DEPLOY_DIR}/exports.sh 

      export REGISTRY=${GITLAB_INNERSOURCE_REGISTRY}:5001
      export STACK_NAME=${APP_NAME}
      export TARGET_HOSTNAME=$host

      ${DEPLOY_DIR}/deploy.sh

      rm -rf ${DEPLOY_DIR}
  "
done