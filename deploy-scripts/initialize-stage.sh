#!/bin/bash

echo "Create Artifacts directory ..."
mkdir -p ${APP_NAME}_ARTIFACTS

echo "Create exports.sh file from EXPORTS variable ..."
for export in "${!EXPORTS@}"; 
do 
  echo "${!export}" | tr "," "\n" | sed 's/^/export /' >> \
    ${APP_NAME}_ARTIFACTS/exports.sh; 
done;

echo "Cloning Repositories ..."
git clone ${GENERIC_APP_REPOSITORY} ${APP_NAME}_ARTIFACTS/generic
git clone ${APP_REPOSITORY} ${APP_NAME}_ARTIFACTS/custom
git clone ${CONFIG_REPOSITORY} ${APP_NAME}_ARTIFACTS/config

echo "Removing .git as this causes problems later in the deploy stage ..."
rm -rf ${APP_NAME}_ARTIFACTS/config/.git ${APP_NAME}_ARTIFACTS/custom/.git \
${APP_NAME}_ARTIFACTS/generic/.git

echo "Reading needed configuration ..."
/home/usgs-user/nvm/versions/node/v10.16.0/bin/node \
container-deploy/deploy-scripts/read-config.js ${ENVIRONMENT} \
${APP_NAME}_ARTIFACTS/config

echo "Move targets file into artifacts directory ..."
mv ${TARGET_FILENAME} ${APP_NAME}_ARTIFACTS/${TARGET_FILENAME}