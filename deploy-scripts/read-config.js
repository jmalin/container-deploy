const fs = require('fs');

const readPath = process.argv[3];
const config = JSON.parse(fs.readFileSync(`${readPath}/config.json`));
const environment = process.argv[2].toLowerCase();
const container = config[environment]['container'];

let targetHosts = [];
for(let k in container) targetHosts.push(k);

fs.writeFileSync('targets.sh', targetHosts.join('\n'));