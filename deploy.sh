#!/bin/bash -e

pushd $(dirname $0) > /dev/null 2>&1;

# Include configuration
test -f './custom.config.sh' && source './custom.config.sh';
source './default.config.sh'; # Provide fallback configuration

# Include functions
test -f './custom.funcs.sh' && source './custom.funcs.sh';
source './default.funcs.sh'; # Provide fallback functions

# Deploy the stack
preStackDeployHook;
docker stack deploy \
  --prune \
  --with-registry-auth \
  --resolve-image always \
  -c ${APP_NAME}.yml \
  ${STACK_NAME};

deployed=$(waitForStackHealthy $STACK_NAME);

if [ $deployed ]; then
  echo "## The ${STACK_NAME} stack is healthy!";
else
  echo "## The ${STACK_NAME} stack failed to deploy. Failing.";
  exit -1;
fi

postStackDeployHook $DATA_LOAD_TYPE;

updateRouting $APP_NAME $STACK_NAME "${SERVICE_MAP[@]}";


popd > /dev/null 2>&1;
exit 0;
