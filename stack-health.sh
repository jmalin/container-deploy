#!/bin/bash -e

STATUS_HEALTHY=0;
STATUS_WARNING=1;
STATUS_CRITICAL=2;
STATUS_UNKNOWN=3;

EXIT_STATUS=-1;
NRPE_OUTPUT='# [ServiceName/Running/Requested/Status]';

setExitStatus () {
  local currentStatus=$1; shift;
  local newStatus=$1; shift;

  if [ $currentStatus -lt $newStatus ]; then
    currentStatus=$newStatus;
  fi

  echo $currentStatus;
}

if [ $# -ge 1 ]; then
  stackNames=$@; shift;
else
  stackNames=$(docker stack ls --format '{{.Name}}');
fi

for stackName in $stackNames; do
  echo "Checking ${stackName}...";

  for service in $(docker stack services --format '{{.Name}}/{{.Replicas}}' ${stackName}); do
    if [ $EXIT_STATUS -eq -1 ]; then
      EXIT_STATUS=$STATUS_HEALTHY;
    fi

    name=$(echo $service | awk -F/ '{print $1}');
    running=$(echo $service | awk -F/ '{print $2}');
    desired=$(echo $service | awk -F/ '{print $3}');
    healthy=$(echo $service | awk -F/ '{print $2/$3}');

    if [ $healthy -ne 1 ]; then
      echo "  - [${service}/unhealthy]";
      NRPE_OUTPUT="${NRPE_OUTPUT}<br/>[${service}/unhealthy]";

      if [ $running -eq 0 ]; then
        EXIT_STATUS=$(setExitStatus $EXIT_STATUS $STATUS_WARNING);
      else
        EXIT_STATUS=$(setExitStatus $EXIT_STATUS $STATUS_CRITICAL);
      fi
    else
      echo "  - [${service}/healthy]";
      NRPE_OUTPUT="${NRPE_OUTPUT}<br/>[${service}/healthy]";
    fi
  done
done

if [ $EXIT_STATUS -eq -1 ]; then
  EXIT_STATUS=$STATUS_UNKNOWN;
fi

echo "${NRPE_OUTPUT[@]}";
exit $EXIT_STATUS;